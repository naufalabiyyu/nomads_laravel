@extends('layouts.checkout')

@section('title', 'Checkout')

@push('prepend-style')
    <link rel="stylesheet" href="{{ url('/frontend/libraries/gijgo/css/gijgo.min.css') }}">
@endpush

@section('content')
    <main>
        <section class="section-details-header"></section>
        <section class="section-details-content">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Paket Travel</li>
                                <li class="breadcrumb-item">Details</li>
                                <li class="breadcrumb-item active">Checkout</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 ps-lg-8">
                        <div class="card card-details">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <h1>Who Is Going?</h1>
                            <p>Trip to {{ $item->travel_package->title }}, {{ $item->travel_package->location }}</p>
                            <div class="attende">
                                <table class="table table-responsive-sm text-center">
                                    <thead>
                                        <tr>
                                            <th>Picture</th>
                                            <th>Name</th>
                                            <th>Nationality</th>
                                            <th>VISA</th>
                                            <th>Passport</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($item->details as $detail)
                                            <tr>
                                            <td>
                                                <img src="https://ui-avatars.com/api/?name={{ $detail->username }}" height="60" class="rounded-circle">
                                            </td>
                                            <td class="align-middle">
                                                {{ $detail->username }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $detail->nationality }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $detail->is_visa ? '30 Days' : 'N/A' }}
                                            </td>
                                            <td class="align-middle">
                                                {{ \Carbon\Carbon::createFromDate($detail->doe_passport) > \Carbon\Carbon::now() ? 'Active' : 'Inactive' }}
                                            </td>
                                            <td class="align-middle">
                                                <a href="{{ route('checkout-remove', $detail->id) }}">
                                                    <img src="{{ url('/frontend/images/Ic_remove.png') }}" alt="">
                                                </a>
                                            </td>
                                        </tr>
                                        @empty
                                           <tr>
                                                <td colspan="6" class="text-center">No Visitor</td>   
                                            </tr>     
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="member mt-3">
                                <h2>Add Member</h2>
                                <form class="form-row" method="post" action="{{ route('checkout-create', $item->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-lg-3">
                                            <label for="username" class="visually-hidden">Name</label>
                                            <input type="text" name="username" class="form-control mb-2 me-sm-2" id="username" placeholder="Username" required>
                                        </div>
                                        <div class="form-group col-lg-2">
                                            <label for="nationality" class="visually-hidden">Nationality</label>
                                            <input type="text" name="nationality" class="form-control mb-2 me-sm-2 w-75" style="margin-left: -10px" id="nationality" placeholder="Nationality" required>
                                        </div>
                                        <div class="form-group col-lg-2">
                                            <label for="is_visa" class="visually-hidden">Visa</label>
                                            <select name="is_visa" class="form-select mb-2 me-sm-2" style="margin-left: -45px" id="is_visa" required>
                                              <option value="" selected>VISA</option>
                                              <option value="1">30 Days</option>
                                              <option value="0">N/A</option>
											</select>
                                        </div>
                                        <div class="form-group col-lg-2">
                                            <label for="doe_passport" class="visually-hidden">DOE Passport</label>
                                            <div class="input-group mb-2 me-sm-2">
                                                <input type="text" class="form-control datepicker" id="doe_passport" style="margin-left: -55px" name="doe_passport" placeholder="DOE Passport" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <button type="submit" class="btn btn-add-now mb-2 px-4">
                                                Add Now
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <h3 class="mt-2 mb-0">Note</h3>
                                <p class="disclaimer mb-0">
                                    You are only able to invite member that has registered in Nomands
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-details card-right">
                            <h2>Checkout Information</h2>
                            <table class="trip-information">
                                <tr>
                                    <th width="50%">Members</th>
                                    <td width="50%" class="text-end">
                                        {{ $item->details->count() }} Person
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Additional VISA</th>
                                    <td width="50%" class="text-end">
                                        $ {{ $item->additional_visa }},00
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Trip Price</th>
                                    <td width="50%" class="text-end">
                                        $ {{ $item->travel_package->price }},00 / Person
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Sub Total</th>
                                    <td width="50%" class="text-end">
                                        $ {{ $item->transaction_total }},00
                                    </td>
                                </tr>
                                <tr>
                                    <th width="50%">Total (+Unique)</th>
                                    <td width="50%" class="text-end text-total">
                                        <span class="text-blue">$ {{ $item->transaction_total }},</span
                                            ><span class="text-orange">{{ mt_rand(0,99) }}</span>
                                    </td>
                                </tr>
                            </table>
                            <hr>
                            <h2>Payment Instruction</h2>
                            <p class="payment-instruction">Please complete the payment before to continue the wonderful trip</p>
                            <div class="bank">
                                <div class="bank-item">
                                    <img src="{{ url('/frontend/images/ic_bank.png') }}" alt="" class="bank-image ">
                                    <div class="description">
                                        <h3>PT Nomads ID</h3>
                                        <p>
                                            0881 8829 8800
                                            <br> Bank Central Asia
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="bank-item">
                                    <img src="{{ url('/frontend/images/ic_bank.png') }}" alt="" class="bank-image ">
                                    <div class="description">
                                        <h3>PT Nomads ID</h3>
                                        <p>
                                            1111 2208 1996
                                            <br> Bank HSBC
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="join-container">
                            <a href="{{ route('success-checkout', $item->id) }}" class="btn w-100 btn-join-now mt-3 py-2">
								I Have Made Payment
							</a>
                        </div>
                        <div class="text-center mt-3">
                            <a href="{{ route('detail', $item->travel_package->slug) }}" class="text-muted">Cancel Booking</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('addon-script')
    <script src="{{ url('/frontend/libraries/gijgo/js/gijgo.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                uiLibrary: 'bootstrap4',
                icons: {
                    rightIcon: '<img src="{{ url('/frontend/images/IC_DOE.png') }}"/>'
                }
            });
        });
    </script>
@endpush