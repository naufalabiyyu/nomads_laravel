<!-- Navbar -->
    <div class="container">
        <nav class="row navbar navbar-expand-lg navbar-light bg-white">
            <div class="container-fluid">
                <div class="navbar-nav ms-auto me-auto me-sm-auto me-lg-0 me-md-auto border-end">
                    <a href="{{ route('home') }}" class="navbar-brand">
                        <img src="{{ url('/frontend/images/logo.png') }}" alt="">
                    </a>
                </div>
                <ul class="navbar-nav me-auto d-none d-lg-block">
                    <li>
                        <span class="text-muted">
						&nbsp; &nbsp; Beyond the explorer of the world
					</span>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- End Navbar -->