<!-- Footer -->
<footer class="section-footer mt-4 mb-4 border-top">
    <div class="container pt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-lg-7">
                        <img src="/frontend/images/logo.png" alt="">
                        <ul class="list-unstyled mt-2">
                            <li><a href="">Address : Jakarta Selatan, Indonesia</a></li>
                            <li><a href="">Phone : 0812 - 222 - 1123 </a></li>
                            <li><a href="">Email: Support@nomads.com</a></li>
                        </ul>
                        <div class="footer-social">
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-12 col-lg-3 ml-auto">
                        <h5>INFORMATION</h5>
                        <ul class="list-unstyled">
                            <li><a href="">About Us</a></li>
                            <li><a href="">Checkout</a></li>
                            <li><a href="">Contact</a></li>
                            <li><a href="">Service</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-2 ml-auto">
                        <h5>ACCOUNT</h5>
                        <ul class="list-unstyled">
                            <li><a href="">My Account</a></li>
                            <li><a href="">Refund</a></li>
                            <li><a href="">Security</a></li>
                            <li><a href="">Rewards</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row border-top justify-content-center align-items-center pt-4">
            <div class="col-auto text-grey-500 font-weight-light">
                2021 Copyright Nomads. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->