<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => '1',
                'name' => 'admin',
                'email' => 'admin@travel.com',
                'password' => bcrypt('admin'),
                'created_at' => now(),
                'updated_at' => now(),
                'remember_token' => Str::random(10),
                'roles' => 'ADMIN',
                'username' => 'naufal',
            ]]);
    }
}
